class Establecimiento{  
    
  
	constructor(idEstablecimiento, nombreEstablecimiento,direccionEstablecimiento,duenoEstablecimiento, aniooEstablecimiento)
	 {
    
		this.idEstablecimiento = idEstablecimiento;
    
		this.nombreEstablecimiento = nombreEstablecimiento;
  
		this.direccionEstablecimiento= direccionEstablecimiento;
		this.dueñoEstablecimiento=duenoEstablecimiento;
		this.añoEstablecimiento=anioEstablecimiento;
	}
  

	getanioEstablecimiento()
	{
		return this.añoEstablecimiento;
	}
	getidEstablecimiento()
	{
      return this.idEstablecimiento;
 
	}

	getnombreEstablecimiento()
	{
		return this.nombreEstablecimiento;
	}
	getdireccionEstablecimiento()
	{
		return this.direccionEstablecimiento;
	}
	getduenoEstablecimiento()
	{
		return this.duenoEstablecimiento;
	}
	setidEstablecimiento(idEstablecimiento)
	{
      this.idEstablecimiento = idEstablecimiento;
  
	}


	setnombreEstablecimiento(nombreEstablecimiento)
	{
      this.nombreEstablecimiento = nombreEstablecimiento;
  
	}

	
	setanioEstablecimiento(anioEstablecimiento)
	{
		this.anioEstablecimiento=anioEstablecimiento;
	}
	setdireccionEstablecimiento(direccionEstablecimiento)
	{
      this.direccionEstablecimiento = direccionEstablecimiento;
  
	}


	setduenoEstablecimiento(dueñoEstablecimiento)
	{
      this.duenoEstablecimiento = dueñoEstablecimiento;
  
	}

}