/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
importarScript("empleadoDP.js");
importarScript("connBD.js");

class empleadoMD{  
    guardarempleadoMD(nuevoEmpleado){
      var idE = nuevoEmpleado.getIdE();
      var arrAtributos;
      if(connBD.verificarId(idE)){
          arrAtributos[0] = nuevoEmpleado.getIdE();
          arrAtributos[1] =  nuevoEmpleado.getNombreE();
          arrAtributos[2] = nuevoEmpleado.getApellidoE();
          arrAtributos[3] = nuevoEmpleado.getEdadE();        
          connBD.guardarempleadoBD(arrAtributos);
      }
  }

    eliminarEmpleadoMD(idEmpleado){
        if(connBD.verficiarId(idEmpleado)){
            eliminarDB(idEmpleado);
        }
    }

    modificarEmpleadoMD(idEmpleado,tabla, campo, atributo){
        if(connBD.verficiarId(idEmpleado)){
            connBD.modificarBD(tabla, campo, atributo);
        }
    }

    consultarEmpleadoMD(idEmpleado){
        if(connBD.verficiarId(idEmpleado)){
            var arrAtributos = connBD.consultarEmpleadoBD(idEmpleado);
            var consulidE = arrAtributos[0];
            var consulnombreE= arrAtributos[1];
            var consulapellidoE= arrAtributos[2];
            var consuledadE = arrAtributos[3];
            var empleadoConsul = empleadoDP();
            empleadoConsul.constructor(consulidE, consulnombreE, consulapellidoE, consuledadE);
            return empleadoConsul;
        }
    }
}


