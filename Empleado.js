importarScript("EmpleadoMD.js");

importarScript("connBD.js");



class EmpleadoMD{  
    
guardarEmpleadoMD(nuevoEmpleado){

var idE = nuevoEmpleado.getidE();
      
var arrAtributos;
      
if(connBD.verficiarCodigo(idE)){
          
arrAtributos[0] = nuevoEmpleado.getidE();
          
arrAtributos[1] =  nuevoEmpleado.nombreE();
          
arrAtributos[2] = nuevoEmpleado.apellidoE();    
arrAtributos[3] = nuevoEmpleado.edadE();  
          
connBD.guardarEmpleadoBD(arrAtributos);
      
}
    
}

    
eliminarEmpleadoMD(idE, tabla){
        
if(connBD.verficiarCodigo(idE)){
            
eliminarDB(tabla, idE);
        
}
    
}

    
modificarEmpleadoMD(idE, tabla, campo, atributo){
        
if(connBD.verficiarCodigo(idE)){
            
connBD.modificarBD(tabla, campo, atributo);
        
}
    
}

    
consultarEmpleadoMD(idE){
        
if(connBD.verficiarCodigo(idE)){
            
var arrAtributos = connBD.consultarEmpleadoBD(idE);
            
var consulidE = arrAtributos[0];
           
var consulnombreE = arrAtributos[1];
            
var consulapellidoE= arrAtributos[2];
            
var empleadoConsul = EmpleadoDP();
                      
return empleadoConsul;
        }
    
}

}

