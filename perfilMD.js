importarScript("perfilMD.js");
importarScript("connBD.js");

class perfilMD{  
    guardarPerfilMD(nuevaPerfil){
      var nickP = nuevaPerfil.getnickP();
      var arrAtributos;
      if(connBD.verficiarCodigo(nickP)){
          arrAtributos[0] = nuevaPerfil.getnickP();
          arrAtributos[1] =  nuevaPerfil.getultimoAccesoP();
          arrAtributos[2] = nuevaPerfil.getimgP();    
          connBD.guardarPerfilBD(arrAtributos);
      }
    }

    eliminarPerfilMD(nickP, tabla){
        if(connBD.verficiarCodigo(nickP)){
            eliminarDB(tabla, nickP);
        }
    }

    modificarPerfilMD(nickP, tabla, campo, atributo){
        if(connBD.verficiarCodigo(nickP)){
            connBD.modificarBD(tabla, campo, atributo);
        }
    }

    consultarPerfilMD(nickP){
        if(connBD.verficiarCodigo(nickP)){
            var arrAtributos = connBD.consultarPerfilBD(nickP);
            var consulnickP = arrAtributos[0];
            var consulultimoAccesoP = arrAtributos[1];
            var consulimgP= arrAtributos[2];
            var cervezaConsul = cervezaDP();
            cervezaConsul.constructor(consulnickP, consulultimoAccesoP, consulimgP);
            return cervezaConsul;
        }
    }
}


