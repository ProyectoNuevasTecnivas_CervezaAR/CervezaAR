CordovaFacebook.logout({
   onSuccess: function() {
      alert("The user is now logged out");
   }
});
 
// Unless you need to wait for the native sdk to do its thing, 
// you dont even really need to use a callback: 
CordovaFacebook.logout();