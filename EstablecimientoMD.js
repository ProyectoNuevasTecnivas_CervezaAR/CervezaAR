/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
importarScript("establecimientoDP.js");
importarScript("connBD.js");

class establecimientoMD{  
    guardarestablecimientoMD(nuevoEstablecimiento){
      var idEst = nuevoEstablecimiento.getidEstablecimiento();
      var arrAtributos;
      if(connBD.verificarId(idEst)){
          arrAtributos[0] = nuevoEstablecimiento.getidEstablecimiento();
          arrAtributos[1] =  nuevoEstablecimiento.getnombreEstablecimiento();
          arrAtributos[2] = nuevoEstablecimiento.getdireccionEstablecimiento();
          arrAtributos[3] = nuevoEstablecimiento.getduenoEstablecimiento(); 
          arrAtributos[4] = nuevoEstablecimiento.getanioEstablecimiento(); 
          connBD.guardarestablecimientoBD(arrAtributos);
      }
    }

    eliminarEstablecimientoMD(idEstablecimiento){
        if(connBD.verficiarId(idEstablecimiento)){
            eliminarDB(idEstablecimiento);
        }
    }

    modificarEstablecimientoMD(idEstablecimiento,tabla, campo, atributo){
        if(connBD.verficiarId(idEstablecimiento)){
            connBD.modificarBD(tabla, campo, atributo);
        }
    }

    consultarEstablecimientoMD(idEstablecimiento){
        if(connBD.verficiarId(idEstablecimiento)){
            var arrAtributos = connBD.consultarEstablecimientoBD(idEstablecimiento);
            var consulidEst = arrAtributos[0];
            var consulnombreEst= arrAtributos[1];
            var consuldireccionEst= arrAtributos[2];
            var consulduenoEst = arrAtributos[3];
            var consulanioEst = arrAtributos[4];
            var establecimientoConsul = establecimientoDP();
            establecimientoConsul.constructor(consulidEst, consulnombreEst, consuldireccionEst, consulduenoEst, consulanioEst);
            return establecimientoConsul;
        }
    }
}


