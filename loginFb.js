CordovaFacebook.login({
   permissions: ['email', 'user_likes'],
   onSuccess: function(result) {
      if(result.declined.length > 0) {
         alert("El usuario no acepto terminos");
      }
      /* ... */
   },
   onFailure: function(result) {
      if(result.cancelled) {
         alert("Sucedio un error");
      } else if(result.error) {
         alert("Hubo un error:" + result.errorLocalized);
      }
   }
});