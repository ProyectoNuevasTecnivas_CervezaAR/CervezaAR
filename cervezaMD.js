importarScript("cervezaDP.js");
importarScript("connBD.js");

class cervezaMD{  
    guardarCervezaMD(nuevaCerveza){
      var nombreC = nuevaCerveza.getNombreC();
      var arrAtributos;
      if(connBD.verficiarCodigo(nombreC)){
          arrAtributos[0] = nuevaCerveza.getNombreC();
          arrAtributos[1] =  nuevaCerveza.getempresaFabricadoraC();
          arrAtributos[2] = nuevaCerveza.getingredientesC();
          arrAtributos[3] = nuevaCerveza.getpaisC();        
          connBD.guardarCervezaBD(arrAtributos);
      }
    }

    eliminarCervezaMD(nombreCerveza){
        if(connBD.verficiarCodigo(nombreCerveza)){
            eliminarDB(nombreCerveza);
        }
    }

    modificarCervezaMD(nombreCerveza,tabla, campo, atributo){
        if(connBD.verficiarCodigo(nombreCerveza)){
            connBD.modificarBD(tabla, campo, atributo);
        }
    }

    consultarCervezaMD(nombreCerveza){
        if(connBD.verficiarCodigo(nombreCerveza)){
            var arrAtributos = connBD.consultarCervezaBD(nombreCerveza);
            var consulNombreC = arrAtributos[0];
            var consulempresaFabricadoraC= arrAtributos[1];
            var consulingredientesC= arrAtributos[2];
            var consulpaisC = arrAtributos[3];
            var cervezaConsul = cervezaDP();
            cervezaConsul.constructor(consulNombreC, consulempresaFabricadoraC, consulingredientesC, consulpaisC);
            return cervezaConsul;
        }
    }
}
