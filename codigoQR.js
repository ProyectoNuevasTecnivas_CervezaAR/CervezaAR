importarScript("cervezaMD.js");
importarScript("cervezaDP.js");

cordova.plugins.barcodeScanner.scan(
   function(result){
        if(!result.cancelled){
            var cerveza = CervezaDP();
               // Se procesa unicamente el código QR
               if(result.format == "QR_CODE"){
                    var nombreCerveza = result.text;
                    cerveza = consultarCervezaMD(nombreCerveza);
                    if(nombreCerveza != null){                       
                        alert("Información de cervezas: /n/ Nombre: "+cerveza.getNombreC() + 
                                "/n/ Empresa: "+cerveza.getempresaFabricadoraC() +
                                "/n/ Ingredientes: "+cerveza.getingredientesC() +
                                " /n/ Pais: "+ cerveza.getpaisC());
                    }
                    else{
                        alert("No se encontró la cerveza");
                    }
                    
               }
               else
                  alert("El código escaneado no es QR, el código es: " + result.format + ". \n\
                            Intente de nuevo.");
        }
        else
          alert("El usuario canceló el escaneo.");
    },
    function(error) {
          alert("Ha ocurrido un error: " + error);
     }
);

//cordova plugin add phonegap-plugin-barcodescanner