class Usuario{  
    
  
	constructor(idUsuario, nombreUsuario,apellidoUsuario,edadUsuario)
	 {
    
		this.idUsuario = idUsuario;
    
		this.nombreUsuario = nombreUsuario;
  
		this.apellidoUsuario= apellidoUsuario;
		this.edadUsuario=edadUsuario;
	}
  


	getidUsuario()
	{
      return this.idUsuario;
 
	}

	getnombreUsuario()
	{
		return this.nombreUsuario;
	}
	getapellidoUsuario()
	{
		return this.apellidoUsuario;
	}
	getedadUsuario()
	{
		return this.edadUsuario;
	}
	setidUsuario(idUsuario)
	{
      this.idUsuario = idUsuario;
  
	}


	setnombreUsuario(nombreUsuario)
	{
      this.nombreUsuario = nombreUsuario;
  
	}


	setapellidoUsuario(apellidoUsuario)
	{
      this.apellidoUsuario = apellidoUsuario;
  
	}


	setedadUsuario(edadUsuario)
	{
      this.edadUsuario = edadUsuario;
  
	}

}