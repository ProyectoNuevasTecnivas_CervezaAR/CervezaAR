/*==============================================================*/
/* Nombre de la BD:      bd_cerveza                             */
/*==============================================================*/


drop table if exists BUSQUEDA;

drop table if exists CERVEZA;

drop table if exists EMPLEADO;

drop table if exists ESTABLECIMIENTO;

drop table if exists ESTABLECIMIENTOXCERVEZA;

drop table if exists LUGARTRABAJO;

drop table if exists PERFIL;

drop table if exists PERFILXUSUARIO;

drop table if exists USUARIO;

/*==============================================================*/
/* Tabla: BUSQUEDA                                              */
/*==============================================================*/
create table BUSQUEDA
(
   IDUSUARIO            int not null,
   IDCERVEZA            int not null,
   FECHABUSQUEDA        datetime not null,
   VECESBUSQUEDA        smallint,
   primary key (IDUSUARIO, IDCERVEZA)
);

/*==============================================================*/
/* Tabla: CERVEZA                                               */
/*==============================================================*/
create table CERVEZA
(
   IDCERVEZA            int not null,
   NOMBRECERVEZA        char(15) not null,
   EMPRESAFABRICADORACERVEZA char(15) not null,
   INGREDIENTESCERVEZA  text not null,
   PAISPROVENIENCIACERVEZA char(15) not null,
   primary key (IDCERVEZA)
);

/*==============================================================*/
/* Tabla: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO
(
   IDEMPLEADO           int not null,
   NOMBREEMPLEADO       char(15) not null,
   APELLIDOEMPLEADO     char(15) not null,
   EDADEMPLEADO         smallint not null,
   primary key (IDEMPLEADO)
);

/*==============================================================*/
/* Tabla: ESTABLECIMIENTO                                       */
/*==============================================================*/
create table ESTABLECIMIENTO
(
   IDESTABLECIMIENTO    int not null,
   NOMBREESTABLECIMIENTO char(15) not null,
   DIRECCIONESTABLECIMIENTO char(40) not null,
   DUENOESTABLECIMIENTO char(30) not null,
   ANOESTABLECIMIENTO   smallint not null,
   primary key (IDESTABLECIMIENTO)
);

/*==============================================================*/
/* Tabla: ESTABLECIMIENTOXCERVEZA                               */
/*==============================================================*/
create table ESTABLECIMIENTOXCERVEZA
(
   IDCERVEZA            int not null,
   IDESTABLECIMIENTO    int not null,
   primary key (IDCERVEZA, IDESTABLECIMIENTO)
);

/*==============================================================*/
/* Tabla: LUGARTRABAJO                                          */
/*==============================================================*/
create table LUGARTRABAJO
(
   IDEMPLEADO           int not null,
   IDESTABLECIMIENTO    int not null,
   INICIOTRABAJO        datetime not null,
   primary key (IDEMPLEADO, IDESTABLECIMIENTO)
);

/*==============================================================*/
/* Tabla: PERFIL                                                */
/*==============================================================*/
create table PERFIL
(
   IDPERFIL             int not null,
   NICKPERFIL           char(15) not null,
   ULTIMOACCESOPERFIL   datetime not null,
   IMAGENPERFIL         longblob,
   primary key (IDPERFIL)
);

/*==============================================================*/
/* Tabla: PERFILXUSUARIO                                        */
/*==============================================================*/
create table PERFILXUSUARIO
(
   IDUSUARIO            int not null,
   IDPERFIL             int not null,
   ULTIMOINGRESO        datetime not null,
   primary key (IDUSUARIO, IDPERFIL)
);

/*==============================================================*/
/* Tabla: USUARIO                                               */
/*==============================================================*/
create table USUARIO
(
   IDUSUARIO            int not null,
   NOMBREUSUARIO        char(15) not null,
   APELLIDOUSUARIO      char(15) not null,
   EDADUSUARIO          smallint not null,
   primary key (IDUSUARIO)
);

alter table BUSQUEDA add constraint FK_OBTIENE_DATOS foreign key (IDCERVEZA)
      references CERVEZA (IDCERVEZA) on delete restrict on update restrict;

alter table BUSQUEDA add constraint FK_REALIZA foreign key (IDUSUARIO)
      references USUARIO (IDUSUARIO) on delete restrict on update restrict;

alter table ESTABLECIMIENTOXCERVEZA add constraint FK_CONTIENE foreign key (IDESTABLECIMIENTO)
      references ESTABLECIMIENTO (IDESTABLECIMIENTO) on delete restrict on update restrict;

alter table ESTABLECIMIENTOXCERVEZA add constraint FK_HAY foreign key (IDCERVEZA)
      references CERVEZA (IDCERVEZA) on delete restrict on update restrict;

alter table LUGARTRABAJO add constraint FK_ES_MIEMBRO_DE foreign key (IDEMPLEADO)
      references EMPLEADO (IDEMPLEADO) on delete restrict on update restrict;

alter table LUGARTRABAJO add constraint FK_TRABAJA_EN foreign key (IDESTABLECIMIENTO)
      references ESTABLECIMIENTO (IDESTABLECIMIENTO) on delete restrict on update restrict;

alter table PERFILXUSUARIO add constraint FK_PERTENECE foreign key (IDPERFIL)
      references PERFIL (IDPERFIL) on delete restrict on update restrict;

alter table PERFILXUSUARIO add constraint FK_TIENE foreign key (IDUSUARIO)
      references USUARIO (IDUSUARIO) on delete restrict on update restrict;

