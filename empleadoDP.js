class empleado{  
    
  constructor(idE, nombreE, apellidoE, edadE) {
    this.idE = idE;
    this.nombreE = nombreE;
    this.apellidoE = apellidoE;
    this.edadE = edadE;
  }
  
  getIdE(){
      return this.idE;
  }
  
  getNombreE(){
      return this.nombreE;
  }
  
  getApellidoE(){
      return this.apellidoE;
  }
  
  getEdadE(){
      return this.edadE;
  }
  
  setIdE(idE){
      this.idE = idE;
  }
  
  setNombreE(nombreE){
      this.nombreE = nombreE;
  }
  
  setApellidoE(apellidoE){
      this.apellidoE = apellidoE;
  }
  
  setEdadE(edadE){
      this.edadE = edadE;
  }
    
}
